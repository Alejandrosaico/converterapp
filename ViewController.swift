//
//  ViewController.swift
//  ConverterApp
//
//  Created by victor saico justo on 9/05/18.
//  Copyright © 2018 victor saico justo. All rights reserved.
//

import UIKit

class ViewController: UIViewController{

    @IBOutlet weak var initialValue: UITextField!
    @IBOutlet weak var fromSegment: UISegmentedControl!
    @IBOutlet weak var toSegment: UISegmentedControl!
    @IBOutlet weak var finalValue: UILabel!
    
    
    
    @IBAction func convertir(_ sender: UIButton) {
        if(Double(initialValue.text!) != nil){
            let numero : Double = Double(initialValue.text!)!
            var resultado : Double = 0.0

            switch(fromSegment.selectedSegmentIndex){
             case 0:
                resultado = millasConverter(inicial: numero, toSegment: toSegment.selectedSegmentIndex)

                finalValue.text = "\(fixData(valor : numero)) \(getType(index: fromSegment.selectedSegmentIndex)) = \(fixData(valor: resultado)) \(getType(index: toSegment.selectedSegmentIndex))"
                break;
            case 1:
                resultado = kmConverter(inicial: numero, toSegment: toSegment.selectedSegmentIndex)
                
                finalValue.text = " \(fixData(valor: numero)) \(getType(index: fromSegment.selectedSegmentIndex)) = \(fixData(valor: resultado)) \(getType(index: toSegment.selectedSegmentIndex))"
                break;
            case 2:
                resultado = yardConverter(inicial: numero, toSegment: toSegment.selectedSegmentIndex)
                
                finalValue.text = " \(fixData(valor: numero)) \(getType(index: fromSegment.selectedSegmentIndex)) \(fixData(valor: resultado)) \(getType(index: toSegment.selectedSegmentIndex))"
                break;
            case 3:
                resultado = piesConverter(inicial: numero, toSegment: toSegment.selectedSegmentIndex)
                
                finalValue.text = " \(fixData(valor: numero)) \(getType(index: fromSegment.selectedSegmentIndex)) \(fixData(valor: resultado)) \(getType(index: toSegment.selectedSegmentIndex))"
                break
            default:
                break;
            }
        }else {
            finalValue.text = "Verficia tu valor"
        }
    }
    func getType(index:Int) -> String {
        var res :String = ""
        switch index {
        case 0:
            res = "Millas"
            break
        case 1:
            res = "Kms"
            break
        case 2:
            res = "Yardas"
            break
        case 3:
            res = "Pies"
        default:
            break
        }
        return res
    }
    
    func yardConverter(inicial:Double, toSegment:Int) -> Double {
        var resultado:Double = 0.0
        switch toSegment {
        case 0:
            resultado = yardtomill(valor: inicial)
            break
        case 1:
            resultado = yardtokm(valor: inicial)
            break
        case 2:
            resultado = inicial
            break
        case 3:
            resultado = yardtopie(valor: inicial)
            break
        default:
            break
        }
        return resultado
    }
    
    func kmConverter(inicial:Double, toSegment:Int) -> Double {
        var resultado:Double = 0.0
        switch toSegment {
        case 0:
            resultado = kmtomill(valor: inicial)
            break
        case 1:
            resultado = inicial
            break
        case 2:
            resultado = kmtoyard(valor : inicial)
            break
        case 3:
            resultado = kmtopie(valor: inicial)
            break
        default:
            break
        }
        return resultado
    }
    
    func piesConverter(inicial:Double, toSegment:Int) -> Double {
        var resultado:Double = 0.0
        switch toSegment {
        case 0:
            resultado = pietomill(valor: inicial)
            break
        case 1:
            resultado = pietokm(valor: inicial)
            break
        case 2:
            resultado = pietoyard(valor: inicial)
            break
        case 3:
            resultado = inicial
            break
        default:
            break
        }
        return resultado
    }
    func millasConverter(inicial:Double, toSegment:Int) -> Double {
        var resultado:Double = 0.0
        switch toSegment {
        case 0:
            resultado = inicial
            break
        case 1:
            resultado = milltokm(valor: inicial)
            break
        case 2:
            resultado = milltoyard(valor: inicial)
            break
        case 3:
            resultado = milltopie(valor: inicial)
        default:
            break
        }
        return resultado
    }
    func fixData(valor:Double) -> String {
        return String(format: "%.2f", valor)
    }
    
    func kmtomill(valor:Double) -> Double {
        return valor*0.621371
    }
    
    func kmtoyard(valor:Double) -> Double {
        return valor*1093.61
    }
    
    func kmtopie(valor:Double) -> Double {
        return valor*3280.84
    }
    
    func milltokm(valor:Double) -> Double {
        return valor * 1.60934
    }
    
    func milltoyard(valor:Double) -> Double {
        return valor*1760
    }
    
    func milltopie(valor:Double) -> Double {
        return valor*5280
    }
    
    func yardtokm(valor:Double) -> Double {
        return valor*0.0009144
    }
    
    func yardtomill(valor:Double) -> Double {
        return valor*0.000568182
    }
    
    func yardtopie(valor:Double) -> Double {
        return valor*3
    }
    
    func pietokm(valor:Double) -> Double {
        return valor*3280.84
    }
    
    func pietomill(valor:Double) -> Double {
        return valor*5280
    }
    
    func pietoyard(valor:Double) -> Double {
        return valor*3
    }
    func setResult(label :UILabel, initialValue:Double, fromValue:Int, toValue:Int, finalValue:Double)
    {
        label.text = "\(fixData(valor: initialValue)) \(getType(index: fromValue)) = \(fixData(valor: finalValue)) \(getType(index: toValue))"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyBoardWhenTap()
    }

    func hideKeyBoardWhenTap() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

